<?php

class Exercise2SelectplatsModel extends Model
{
    public function selectPlats($i)
    {
        $j = $i-1;
$query = <<<QUERY
SELECT
    nom,tipus,image
FROM
    plat
LIMIT
    1
OFFSET
    $j
QUERY;
       $result = $this->getAll( $query );

       return $result;
    }


    public function getTotalPlats()
    {
        $query = <<<QUERY
SELECT
    COUNT(*)
FROM
    plat
QUERY;
        $result = $this->getAll( $query );

        return $result;

    }

}