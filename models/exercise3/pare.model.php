<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 9/3/15
 * Time: 16:18
 */

class Exercise3PareModel extends Model
{
    public function showAmountTypes()
    {
$query = <<<QUERY
SELECT
    COUNT(*)
FROM
    plat
GROUP BY
    tipus
QUERY;

        $result = $this->getAll( $query );

        return $result;
    }

    public function selectPlats($tipus,$fila)
    {
        $fil = $fila-1;
        $query = <<<QUERY
SELECT
    nom,tipus,image
FROM
    plat
WHERE
    tipus='$tipus'
LIMIT
    3
OFFSET
    $fil
QUERY;

        $result = $this->getAll($query);

        return $result;

    }

    public function selectNomPlats()
    {

        $query = <<<QUERY
SELECT
    nom,image
FROM
    plat
QUERY;
        $result = $this->getAll( $query );
        return $result;
    }
}