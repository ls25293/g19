<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 9/3/15
 * Time: 16:18
 */

class Exercise4PareModel extends Model
{
    public function showAmountTypes()
    {
$query = <<<QUERY
SELECT
    COUNT(*)
FROM
    plat
GROUP BY
    tipus
QUERY;

        $result = $this->getAll( $query );

        return $result;
    }

    public function selectPlats($tipus,$fila)
    {
        $fil = $fila-1;
        $query = <<<QUERY
SELECT
    nom,tipus,image
FROM
    plat
WHERE
    tipus='$tipus'
LIMIT
    3
OFFSET
    $fil
QUERY;

        $result = $this->getAll($query);

        return $result;

    }

    public function selectNomPlats()
    {

        $query = <<<QUERY
SELECT
    id_plat,nom,tipus,image
FROM
    plat
QUERY;
        $result = $this->getAll( $query );
        return $result;
    }

    public function getNomTipusURLPlat($id)
    {

        $query = <<<QUERY
SELECT
    nom,tipus,image
FROM
    plat
WHERE
    id_plat=$id
QUERY;
        $result = $this->getAll( $query );
        return $result;

    }

    public function updateCarta($id,$nom,$tipus,$image)
    {

        $query = <<<QUERY
UPDATE
    plat
SET
    nom='$nom', tipus='$tipus', image='$image'
WHERE
    id_plat=$id
QUERY;

        $this->execute( $query );
    }


    public function guardaPlato($name,$type,$url)
    {

        $query = <<<QUERY
INSERT INTO
    plat
VALUES
    (NULL,'$name','$type','$url')
QUERY;

        $this->execute($query);
    }


    public function esborraPlat($id)
    {

        $query = <<<QUERY
DELETE FROM
    plat
WHERE
    id_plat=$id
QUERY;

        $this->execute( $query );

    }

}