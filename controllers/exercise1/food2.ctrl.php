<?php
/**
 * Home Controller: Controller example.

 */
class Exercise1DessertsController extends Controller
{
	protected $view = 'exercise1/food.tpl';

	public function build()
	{

        $params = $this->getParams();
        $par = $params['url_arguments'][0];
        $previouspar = $par-1;
        $nextpar = $par+1;
        $image = 'Des' . $par . '.jpg';

        if ($par == 1 || $par == 2 || $par == 3 || $par == 4 || $par == 5) {

            $this->setLayout( $this->view );
            $this->assign('foodtype','POSTRES');
            $this->assign('linkimage', $image);

            if ($par == 1) {
                $this->assign('visprev','visible');
                $this->assign('visnext','visible');
                $this->assign('previouslink', 'http://g19.local/food/5');
                $this->assign('nextlink', $nextpar);
                $this->assign('urlimage','http://www.southernliving.com');
                //$this->assign('visprev',"hidden");
            } else if ($par == 2) {
                $this->assign('visprev','visible');
                $this->assign('visnext','visible');
                $this->assign('previouslink', $previouspar);
                $this->assign('nextlink', $nextpar);
                $this->assign('urlimage','http://www.cookinglight.com');
            } else if ($par == 3) {
                $this->assign('visprev','visible');
                $this->assign('visnext','visible');
                $this->assign('previouslink', $previouspar);
                $this->assign('nextlink', $nextpar);
                $this->assign('urlimage','http://www.lifestylefood.com.au');
            } else if ($par == 4) {
                $this->assign('visprev','visible');
                $this->assign('visnext','visible');
                $this->assign('previouslink', $previouspar);
                $this->assign('nextlink', $nextpar);
                $this->assign('urlimage','http://www.bettycrocker.com');
            } else {
                $this->assign('visprev','visible');
                $this->assign('visnext','hidden');
                $this->assign('previouslink', $previouspar);
                $this->assign('urlimage','http://dietketo.com');
            }

        } else {

            $this->setLayout( 'error/error404.tpl' );

        }
        /*
         *
         * visibility: $visprev; #previous { action: previouslink; } #next { action: nextlink; }
         */
	
	}

    /**
	 * With this method you can load other modules that we will need in our page. You will have these modules availables in your template inside the "modules" array (example: {$modules.head}).
	 * The sintax is the following:
	 * $modules['name_in_the_modules_array_of_Smarty_template'] = Controller_name_to_load;
	 *
	 * @return array
	 */
	public function loadModules() {
		$modules['head']	= 'SharedHeadController';
		$modules['footer']	= 'SharedFooterController';
		return $modules;
	}
}
