<?php
/**
 * Home Controller: Controller example.

 */
class Exercise1EntrantsController extends Controller
{
	protected $view = 'exercise1/food.tpl';

	public function build()
	{

        $params = $this->getParams();
        $par = $params['url_arguments'][0];
        $previouspar = $par-1;
        $nextpar = $par+1;
        $image = 'Ent' . $par . '.jpg';

        if ($par == 1 || $par == 2 || $par == 3 || $par == 4 || $par == 5)
        {
            $this->setLayout( $this->view );
            $this->assign('foodtype','ENTRANTES');
            $this->assign('linkimage', $image);

            if ($par == 1)
            {
                $this->assign('visprev','hidden');
                $this->assign('visnext','visible');
                $this->assign('nextlink', $nextpar);
                $this->assign('urlimage','http://allrecipes.com/');
            } else if ($par == 2)
            {
                $this->assign('visprev','visible');
                $this->assign('visnext','visible');
                $this->assign('previouslink', $previouspar);
                $this->assign('nextlink', $nextpar);
                $this->assign('urlimage','http://www.food.com/');
            } else if ($par == 3)
            {
                $this->assign('visprev','visible');
                $this->assign('visnext','visible');
                $this->assign('previouslink', $previouspar);
                $this->assign('nextlink', $nextpar);
                $this->assign('urlimage','http://www.foodnetwork.com/recipes.html');
            } else if ($par == 4)
            {
                $this->assign('visprev','visible');
                $this->assign('visnext','visible');
                $this->assign('previouslink', $previouspar);
                $this->assign('nextlink', $nextpar);
                $this->assign('urlimage','http://www.jamieoliver.com/recipes/');
            }

            else {
                $this->assign('visprev','visible');
                $this->assign('visnext','visible');
                $this->assign('previouslink', $previouspar);
                $this->assign('nextlink', 'http://g19.local/food2/1');
                $this->assign('urlimage', 'http://www.realsimple.com/food-recipes');
            }

        } else {

            $this->setLayout( 'error/error404.tpl' );

        }
        /*
         *
         * visibility: $visprev; #previous { action: previouslink; } #next { action: nextlink; }
         */
	
	}

    /**
	 * With this method you can load other modules that we will need in our page. You will have these modules availables in your template inside the "modules" array (example: {$modules.head}).
	 * The sintax is the following:
	 * $modules['name_in_the_modules_array_of_Smarty_template'] = Controller_name_to_load;
	 *
	 * @return array
	 */
	public function loadModules() {
		$modules['head']	= 'SharedHeadController';
		$modules['footer']	= 'SharedFooterController';
		return $modules;
	}
}
