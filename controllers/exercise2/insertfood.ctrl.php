<?php


class Exercise2InsertFoodController extends Controller
{
    protected $view = 'exercise2/insertfood.tpl';

    public function build()
    {
        $check = $this->getParams();
        if (isset($check['url_arguments']) && count($check['url_arguments']) == 1
                && (( $check['url_arguments'][0] == "insert")
                || $check['url_arguments'][0] == "inici")) {
            $obj = $this->getClass('Exercise2InsertFoodModel');
            $this->setLayout($this->view);
            if ($check['url_arguments'][0] == 'insert') {
                $nombre = Filter::getString('namefood');
                $tipo = Filter::getString('typefood');
                $urlplato = Filter::getUrl('urlfood');
                $obj->guardaPlato($nombre, $tipo, $urlplato);
            }
        } else {
            $this->setLayout('error/error404.tpl');
        }

    }

    public function loadModules() {
        $modules['head']	= 'SharedHeadController';
        $modules['footer']	= 'SharedFooterController';
        return $modules;
    }
}