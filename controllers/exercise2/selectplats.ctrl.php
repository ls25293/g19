<?php

class Exercise2PlatoController extends Controller
{
    protected $view = 'exercise2/plato.tpl';

    public function build()
    {
        $check = $this->getParams();

        if (isset($check['url_arguments']) && count($check['url_arguments']) == 1
            && ctype_digit($check['url_arguments'][0]) && $check['url_arguments'][0] != 0) {
            $i = $check['url_arguments'][0];
            $obj = $this->getClass('Exercise2SelectplatsModel');
            $result = $obj->selectPlats($i);
            $total = $obj->getTotalPlats();
            if (!isset($total[0]['COUNT(*)']) || (isset($total[0]['COUNT(*)']) && $i > $total[0]['COUNT(*)'])) {
                $this->setLayout('error/error404.tpl');
            } else {
                $this->setLayout( $this->view );
                $this->assign('nom', $result[0]['nom']);
                $this->assign('tipus', $result[0]['tipus']);
                $this->assign('image', $result[0]['image']);
                $array=parse_url($result[0]['image']);
                $this->assign('link', $array['scheme'].'://'.$array['host']);
                if ($i == 1) {
                    $this->assign('previouslink', '');
                    $this->assign('visprev','hidden');
                } else {
                    $this->assign('previouslink',$i-1);
                    $this->assign('visprev','visible');
                }
                if ($i == $total[0]['COUNT(*)']) {
                    $this->assign('nextlink', '');
                    $this->assign('visnext','hidden');
                } else {
                    $this->assign('nextlink', $i+1);
                    $this->assign('visnext','visible');
                }
            }
        }else{
            $this->setLayout('error/error404.tpl');
        }
    }

    /**
     * With this method you can load other modules that we will need in our page. You will have these modules availables in your template inside the "modules" array (example: {$modules.head}).
     * The sintax is the following:
     * $modules['name_in_the_modules_array_of_Smarty_template'] = Controller_name_to_load;
     *
     * @return array
     */
    public function loadModules() {
        $modules['head']	= 'SharedHeadController';
        $modules['footer']	= 'SharedFooterController';
        return $modules;
    }
}