<?php

include_once(PATH_CONTROLLERS . 'exercise4/insertlistfood.ctrl.php');

class Exercise4EditFoodController extends Exercise4InsertDelEditFoodController
{

    protected $typeact = 'edit/done/';
    protected $typebutton = 'Edit';

}