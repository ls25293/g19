<?php

include_once(PATH_CONTROLLERS . 'exercise4/dishes.ctrl.php');

class Exercise4DishesPostresController extends Exercise4DishesController
{
    protected $view = 'exercise4/postresboxes.tpl';
    protected $tipus_plat = 'Postres';
    protected $fila_plat = 'postres';
    
}