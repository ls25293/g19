<?php


class Exercise4InsertDelEditFoodController extends Controller
{
    protected $view = 'exercise4/insertdeleditfood.tpl';
    protected $typeact;
    protected $typebutton;

    public function build()
    {
        $check = $this->getParams();
        if (($check['key_main_controller'] == 'insert' && !isset($check['url_arguments']))
            || ($check['key_main_controller'] == 'edit'
                && isset($check['url_arguments'])
                && count($check['url_arguments'] == 2)
                && ($check['url_arguments'][0] == 'standby' || $check['url_arguments'][0] == 'done')
                && (ctype_digit($check['url_arguments'][1])))) {

            //if (isset($check['url_arguments'])
               // && ((count($check['url_arguments']) == 1 && $check['url_arguments'][0] == "insert")
                  //  || (count($check['url_arguments']) == 3 && $check['url_arguments'][0] == "edit"
                   //     && ($check['url_arguments'][1] == 'standby' || $check['url_arguments'][1] == 'done')
                   //     && ctype_digit($check['url_arguments'][2])))){
            //if (isset($check['url_arguments']) && count($check['url_arguments']) == 1
               // && (( $check['url_arguments'][0] == "insert")
                   // || $check['url_arguments'][0] == "inici")) {
            $obj = $this->getClass('Exercise4PareModel');
            $this->setLayout($this->view);
            if ($check['key_main_controller'] == 'insert') {
                $this->assign('typeofaction',$this->typeact);
                $this->assign('typeofbutton',$this->typebutton);
                $nombre = Filter::getString('namefood');
                $tipo = Filter::getString('typefood');
                $urlplato = Filter::getUrl('urlfood');
                if ($nombre != '' && $tipo != '' && $urlplato != '') {
                    $obj->guardaPlato($nombre, $tipo, $urlplato);
                }
            } else if ($check['key_main_controller'] == 'edit') {
                $this->assign('typeofaction',$this->typeact.$check['url_arguments'][1]);
                $this->assign('typeofbutton',$this->typebutton);
                if ($check['url_arguments'][0] == 'done')
                {
                    $nom = Filter::getString('namefood');
                    $tipus = Filter::getString('typefood');
                    $image = Filter::getURL('urlfood');
                    if ($nom != '' && $tipus != 'typefood' && $image != '') {
                        $obj->updateCarta($check['url_arguments'][1],$nom,$tipus,$image);
                    }
                }
                $arrayedit = $obj->getNomTipusURLPlat($check['url_arguments'][1]);
                if (isset($arrayedit[0]['nom']) && isset($arrayedit[0]['image']) && isset($arrayedit[0]['tipus'])) {
                    $this->assign('contentname',$arrayedit[0]['nom']);
                    $this->assign('contenturl',$arrayedit[0]['image']);
                    if ($arrayedit[0]['tipus'] == 'Primer Plato') {
                        $this->assign('selected1','selected');
                    } else if ($arrayedit[0]['tipus'] == 'Segundo Plato') {
                        $this->assign('selected2','selected');
                    } else if ($arrayedit[0]['tipus'] == 'Postres') {
                        $this->assign('selected3','selected');
                    }
                }
            }
            $mod = $this->getClass ( 'Exercise4PareModel' );
            $aux = $mod->selectNomPlats();
            if (isset($aux[0]['nom']))
            {
                $aux = $mod->selectNomPlats();
                for ($b = 0; $b < count($aux); $b++)
                {
                    $array=parse_url($aux[$b]['image']);
                    $ch = $array['scheme'].'://'.$array['host'];
                    $aux[$b]['image'] = $ch;
                    if ($aux[$b]['tipus'] == 'Primer Plato') $aux[$b]['tipus'] = '1r.';
                    else if ($aux[$b]['tipus'] == 'Segundo Plato') $aux[$b]['tipus'] = '2o.';
                    else $aux[$b]['tipus'] = 'P.';
                }
                $this->assign('aux',$aux);
                $this->assign('visgallery','visible');
            } else {
                $this->assign('visgallery','hidden');
            }
        } else {
            $this->setLayout('error/error404.tpl');
        }

    }

    public function loadModules() {
        $modules['head']	= 'SharedHeadController';
        $modules['footer']	= 'SharedFooterController';
        return $modules;
    }
}