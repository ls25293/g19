<?php

class Exercise4EsborraPlatController extends Controller
{

    //protected $view = 'exercise4/insertlistfood.tpl';

    public function build()
    {
        $check = $this->getParams();
        if (isset($check['url_arguments']) && count($check['url_arguments']) == 1
            && ctype_digit($check['url_arguments'][0]) && $check['url_arguments'][0] != 0) {
            $obj = $this->getClass('Exercise4PareModel');
            $obj->esborraPlat($check['url_arguments'][0]);
            //$this->setLayout($this->view);
        } else {
            $this->setLayout('error/error404.tpl');
        }
        header("Location: http://g19.local/insert");
        exit();
    }

}