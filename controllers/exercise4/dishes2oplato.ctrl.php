<?php

include_once(PATH_CONTROLLERS . 'exercise4/dishes.ctrl.php');

class Exercise4Dishes2oPlatoController extends Exercise4DishesController
{
    protected $view = 'exercise4/2oplato.tpl';
    protected $tipus_plat = 'Segundo Plato';
    protected $fila_plat = '2oplato';
    
}