<?php

include_once(PATH_CONTROLLERS . 'exercise4/dishes.ctrl.php');

class Exercise4Dishes1rPlatoController extends Exercise4DishesController
{
    protected $view = 'exercise4/1rplatboxes.tpl';
    protected $tipus_plat = 'Primer Plato';
    protected $fila_plat = '1rplato';
}