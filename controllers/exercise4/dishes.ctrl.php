<?php

class Exercise4DishesController extends Controller
{
    protected $view;
    protected $tipus_plat;
    protected $fila_plat;

    public function build()
    {
        $check = $this->getParams();

        if (isset($check['url_arguments']) && count($check['url_arguments']) == 1
            && ctype_digit($check['url_arguments'][0]) && $check['url_arguments'][0] != 0) {
            $i = $check['url_arguments'][0];
            $obj = $this->getClass('Exercise3PareModel');
            $result = $obj->selectPlats($this->tipus_plat,1+($i-1)*3);
            $this->setLayout( $this->view );
            for ($j = 1; $j <= 3; $j++)
            {
                $urlvalue = 'url'.$this->fila_plat.$j;
                $imgvalue = 'img'.$this->fila_plat.$j;
                if ($j <= count($result))
                {
                    $this->assign($imgvalue,$result[$j-1]['image']);
                    $array=parse_url($result[$j-1]['image']);
                    $this->assign($urlvalue,$array['scheme'].'://'.$array['host']);
                } else {
                    $this->assign($imgvalue,'http://g19.local/imag/No_Image_Available.png');
                    $this->assign($urlvalue.$j,'');
                }
            }

        }else{
            $this->setLayout('error/error404.tpl');
        }
    }

    /**
     * With this method you can load other modules that we will need in our page. You will have these modules availables in your template inside the "modules" array (example: {$modules.head}).
     * The sintax is the following:
     * $modules['name_in_the_modules_array_of_Smarty_template'] = Controller_name_to_load;
     *
     * @return array
     */
    public function loadModules() {
        $modules['head']	= 'SharedHeadController';
        $modules['footer']	= 'SharedFooterController';
        return $modules;
    }
}