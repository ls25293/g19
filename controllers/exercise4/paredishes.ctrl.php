<?php



class Exercise4PareDishesController extends Controller
{
    protected $view = 'exercise4/pare.tpl';

    public function build()
    {
        $check = $this->getParams();
        if (isset($check['url_arguments']) && count($check['url_arguments']) == 1
            && ctype_digit($check['url_arguments'][0]) && $check['url_arguments'][0] != 0) {
            $obj = $this->getClass('Exercise3PareModel');
            $array = $obj->showAmountTypes();
            $i = 0;
            if (isset($array[0]['COUNT(*)'])) $i = $array[0]['COUNT(*)'];
            if (isset($array[1]['COUNT(*)']) && $i < $array[1]['COUNT(*)']) $i = $array[1]['COUNT(*)'];
            if (isset($array[2]['COUNT(*)']) && $i < $array[2]['COUNT(*)']) $i = $array[2]['COUNT(*)'];
            $j = $check['url_arguments'][0];

            if ($i > 0 && $i >= (1+($j-1)*3)) {
                $this->setLayout($this->view);
                if ($j == 1) {
                    $this->assign('previouslink', '');
                    $this->assign('visprev', 'hidden');
                } else {
                    $this->assign('previouslink', $j - 1);
                    $this->assign('visprev', 'visible');
                }

                if ($i < (1 + $j * 3)) {
                    $this->assign('nextlink', '');
                    $this->assign('visnext', 'hidden');
                } else {
                    $this->assign('nextlink', $j + 1);
                    $this->assign('visnext', 'visible');
                }
            } else {
                $this->setLayout('error/error404.tpl');
            }

        } else {
            $this->setLayout('error/error404.tpl');
        }

    }

    public function loadModules() {
        $modules['head']	= 'SharedHeadController';
        $modules['dishes1r'] = 'Exercise4Dishes1rPlatoController';
        $modules['dishes2o']  = 'Exercise4Dishes2oPlatoController';
        $modules['dishespostres'] = 'Exercise4DishesPostresController';
        $modules['footer']	= 'SharedFooterController';
        return $modules;
    }
}