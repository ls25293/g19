<?php

class Exercise32oplatoController extends Controller
{
    protected $view = 'exercise3/2oplato.tpl';

    public function build()
    {
        $check = $this->getParams();

        if (isset($check['url_arguments']) && count($check['url_arguments']) == 1
            && ctype_digit($check['url_arguments'][0]) && $check['url_arguments'][0] != 0) {
            $i = $check['url_arguments'][0];
            $obj = $this->getClass('Exercise3PareModel');
            $result = $obj->selectPlats("Segundo Plato",1+($i-1)*3);
            $this->setLayout( $this->view );
            for ($j = 1; $j <= 3; $j++)
            {
                $urlvalue = 'url2oplato'.$j;
                $imgvalue = 'img2oplato'.$j;
                if ($j <= count($result))
                {
                    $this->assign($imgvalue,$result[$j-1]['image']);
                    $array=parse_url($result[$j-1]['image']);
                    $this->assign($urlvalue,$array['scheme'].'://'.$array['host']);
                } else {
                    $this->assign($imgvalue,'http://g19.local/imag/No_Image_Available.png');
                    $this->assign($urlvalue.$j,'');
                }
            }

        }else{
            $this->setLayout('error/error404.tpl');
        }
    }
}
/*
    public function loadModules() {
        $modules['head']	= 'SharedHeadController';
        $modules['footer']	= 'SharedFooterController';
        return $modules;
    }

}*/