<?php


class Exercise3InsertListFoodController extends Controller
{
    protected $view = 'exercise3/insertlistfood.tpl';

    public function build()
    {
        $check = $this->getParams();
        if (isset($check['url_arguments']) && count($check['url_arguments']) == 1
                && (( $check['url_arguments'][0] == "insert")
                || $check['url_arguments'][0] == "inici")) {
            $obj = $this->getClass('Exercise2InsertFoodModel');
            $this->setLayout($this->view);
            if ($check['url_arguments'][0] == 'insert') {
                $nombre = Filter::getString('namefood');
                $tipo = Filter::getString('typefood');
                $urlplato = Filter::getUrl('urlfood');
                $obj->guardaPlato($nombre, $tipo, $urlplato);
            }
            $mod = $this->getClass ( 'Exercise3PareModel' );
            $aux = $mod->selectNomPlats();
            if (isset($aux[0]['nom']))
            {
                $aux = $mod->selectNomPlats();
                for ($b = 0; $b < count($aux); $b++)
                {
                    $array=parse_url($aux[$b]['image']);
                    $ch = $array['scheme'].'://'.$array['host'];
                    $aux[$b]['image'] = $ch;
                }
                $this->assign('aux',$aux);
                $this->assign('visgallery','visible');
            } else {
                $this->assign('visgallery','hidden');
            }
        } else {
            $this->setLayout('error/error404.tpl');
        }

    }

    public function loadModules() {
        $modules['head']	= 'SharedHeadController';
        $modules['footer']	= 'SharedFooterController';
        return $modules;
    }
}