{$modules.head}

<h1>NOM: {$nom}</h1>
<h1>TIPUS: {$tipus}</h1>

<div>
    <a href={$link}>
        <img id="food_image" src={$image} alt="none">
    </a>
</div>

<form action={$previouslink}>
    <input id="previous" style="visibility:{$visprev}" type="submit" value="Previous Food">
</form>

<form action={$nextlink}>
    <input id="next" style="visibility:{$visnext}" type="submit" value="Next Food">
</form>

{$modules.footer}