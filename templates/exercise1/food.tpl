{$modules.head}

	<!-- This is an HTML comment -->
	{* This is a Smarty comment *}

    <h1>{$foodtype}</h1>

    <div>
        <a href={$urlimage}>
            <img id="food_image" src="{$url.global}/imag/{$linkimage}" alt="none">
        </a>
    </div>

    <form action={$previouslink}>
        <input id="previous" style="visibility:{$visprev}" type="submit" value="Previous Food">
    </form>

    <form action={$nextlink}>
        <input id="next" style="visibility:{$visnext}" type="submit" value="Next Food">
    </form>


{$modules.footer}