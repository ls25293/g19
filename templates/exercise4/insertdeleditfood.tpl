{$modules.head}

	<!-- This is an HTML comment -->
	{* This is a Smarty comment *}

    <h1>{$foodtype}</h1>

    <form action="http://g19.local/{$typeofaction}" method="POST">
        <label>Name:</label><input type="text" name="namefood" value="{$contentname}" required></br>
        </br>
        <label>Type:</label>
        <select name="typefood">
            <option value="Primer Plato" {$selected1}>Primer Plato</option>
            <option value="Segundo Plato" {$selected2}>Segundo Plato</option>
            <option value="Postres" {$selected3}>Postres</option>
        </select></br>
        </br>
        <label>URL:</label><input type="url" name="urlfood" value="{$contenturl}" required></br>
        </br>
        <input type="submit" value="{$typeofbutton}">
    </form>

    <form action="http://g19.local/pare/1" method="POST">
        <input style="visibility:{$visgallery}" type="submit" value="Go to gallery">
    </form>

    <section id="listrecipes">
        <h1 align="center">NUESTRA CARTA</h1>
        <div>
            {foreach from=$aux item=v}
                <ul id="ulcarta">
                    <li id="licarta"><a id="namerecipes" href="{$v.image}">{$v.tipus} {$v.nom}</a></li>
                    <li id="licarta"><a id="namerecipes" href="http://g19.local/edit/standby/{$v.id_plat}">Editar</a></li>
                    <li id="licarta"><a id="namerecipes" href="http://g19.local/delete/{$v.id_plat}">Borrar</a><br/></li>
                </ul>
            {/foreach}
        </div>
    </section>

{$modules.footer}