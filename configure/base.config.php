<?php
/*
 * Archivo de configuraci�n de las clases que usaremos
 * Se llama desde Configure::getClass('NombreClase');
 */

/**
 * Engine:  Don't modify.
 */
//$config['factory']						=  PATH_ENGINE . 'factory.class.php';
//$config['sql']							=  PATH_ENGINE . 'sql.class.php';


$config['mail']							=  PATH_ENGINE . 'mail.class.php';
$config['session']						=  PATH_ENGINE . 'session.class.php';
$config['user']							=  PATH_ENGINE . 'user.class.php';
$config['url']							=  PATH_ENGINE . 'url.class.php';
$config['uploader']						=  PATH_ENGINE . 'uploader.class.php';


$config['dispatcher']					=  PATH_ENGINE . 'dispatcher.class.php';


/** 
 * Controllers & Models
 *
 */

// Models
$config['Exercise2InsertFoodModel']     = PATH_MODELS . 'exercise2/insertfood.model.php';
$config['Exercise2SelectplatsModel']    = PATH_MODELS . 'exercise2/selectplats.model.php';
$config['Exercise3PareModel']           = PATH_MODELS . 'exercise3/pare.model.php';
$config['Exercise4PareModel']           = PATH_MODELS . 'exercise4/pare.model.php';

// 404 Controller
$config['ErrorError404Controller']		= PATH_CONTROLLERS . 'error/error404.ctrl.php';

// Home Controller

$config['HomeHomeController']			  = PATH_CONTROLLERS . 'home/home.ctrl.php';
$config['Exercise1HomeController']        = PATH_CONTROLLERS . 'exercise1/home.ctrl.php';
$config['Exercise1EntrantsController']    = PATH_CONTROLLERS . 'exercise1/food.ctrl.php';
$config['Exercise1DessertsController']    = PATH_CONTROLLERS . 'exercise1/food2.ctrl.php';
$config['Exercise2InsertFoodController']  = PATH_CONTROLLERS . 'exercise2/insertfood.ctrl.php';
$config['Exercise2PlatoController']       = PATH_CONTROLLERS . 'exercise2/selectplats.ctrl.php';
$config['Exercise3PrimerPlatSelectController'] = PATH_CONTROLLERS . 'exercise3/1rplatselect.ctrl.php';
$config['Exercise3InsertListFoodController'] = PATH_CONTROLLERS . 'exercise3/insertlistfood.ctrl.php';
$config['Exercise32oplatoController']       = PATH_CONTROLLERS . 'exercise3/2oplato.ctrl.php';
$config['Exercise3PostresSelectController'] = PATH_CONTROLLERS . 'exercise3/postresselect.ctrl.php';
$config['Exercise3PareController']        = PATH_CONTROLLERS . 'exercise3/pare.ctrl.php';
$config['Exercise4InsertDelEditFoodController'] = PATH_CONTROLLERS . 'exercise4/insertlistfood.ctrl.php';
$config['Exercise4PareDishesController']    = PATH_CONTROLLERS . 'exercise4/paredishes.ctrl.php';
$config['Exercise4DishesController']        = PATH_CONTROLLERS . 'exercise4/dishes.ctrl.php';
$config['Exercise4Dishes2oPlatoController'] = PATH_CONTROLLERS . 'exercise4/dishes2oplato.ctrl.php';
$config['Exercise4Dishes1rPlatoController'] = PATH_CONTROLLERS . 'exercise4/dishes1rplato.ctrl.php';
$config['Exercise4DishesPostresController'] = PATH_CONTROLLERS . 'exercise4/dishespostres.ctrl.php';
$config['Exercise4EsborraPlatController']   = PATH_CONTROLLERS . 'exercise4/esborraplat.ctrl.php';
$config['Exercise4EditFoodController']      = PATH_CONTROLLERS . 'exercise4/editfood.ctrl.php';
$config['Exercise4InsertFoodController']    = PATH_CONTROLLERS . 'exercise4/insertfood.ctrl.php';

// Shared controllers
$config['SharedHeadController']			= PATH_CONTROLLERS . 'shared/head.ctrl.php';
$config['SharedFooterController']		= PATH_CONTROLLERS . 'shared/footer.ctrl.php';