<?php
/*
 * Cada ruta SEO es mapeada a un controlador que se encuentra en el 'base.config.php'
 */

//$config['default']				= 'HomeHomeController';
//$config['home']					= 'HomeHomeController';
$config['default']                  = 'Exercise1HomeController';
$config['home']                     = 'Exercise1HomeController';
$config['food']                     = 'Exercise1EntrantsController';
$config['food2']                    = 'Exercise1DessertsController';
$config['insertfood']               = 'Exercise2InsertFoodController';
$config['selectplats']              = 'Exercise2PlatoController';
$config['insertlistfood']           = 'Exercise3InsertListFoodController';
$config['pare']                     = 'Exercise3PareController';
$config['paredishes']               = 'Exercise4PareDishesController';
$config['delete']                   = 'Exercise4EsborraPlatController';
$config['edit']                   = 'Exercise4EditFoodController';
$config['insert']                   = 'Exercise4InsertFoodController';